defmodule AddLetters do
  def add_letters(letters) do
    if is_list(letters) and length(letters) > 0 do
      letters_to_int_sum(letters)
      |> rem(26)
      |> num_to_letter
    else
      "z"
    end
  end

  defp letters_to_int_sum(letters) do
    letters
    |> Stream.map(fn letter -> (letter |> String.to_charlist |> hd) - ('a'|> hd) + 1 end)
    |> Enum.sum
  end

  defp num_to_letter(0), do: "z"

  defp num_to_letter(num) do
    List.to_string([num + ('a'|> hd) - 1])
  end
end

add_letters = &AddLetters.add_letters/1
IO.puts(add_letters.(["a", "b", "c"]))
IO.puts(add_letters.(["a", "b"]))
IO.puts(add_letters.(["z"]))
IO.puts(add_letters.(["z", "a"]))
IO.puts(add_letters.(["y", "c", "b"]))
IO.puts(add_letters.([]))
IO.puts(add_letters.(["q", "s", "p", "u", "u", "d", "s", "f", "g"]))